// Both cases also saved the previous state
// When the user clicked outside, modal will close
var fadeTime = 300;
var shippingCharge = 15.000;

window.onclick = function (e) {
    if (e.target == $('#modal')[0]) {
        $('#modal').hide();
    }
    if (e.target == $('.forgot-password')[0]) {
        $('.forgot-password').hide();
    }

}
// When the user press ESC, modal will close
$(document).keydown(function (e) {
    if (e.keyCode == 27) {
        $('#modal').hide();
        $('.forgot-password').hide();
    }
});

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks-product");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(document).ready(function () {
    function activeTab(obj) {
        $('.tablinks').removeClass('active');
        $(obj).addClass('active');
        var id = $(obj).find('a').attr('href');
        $('.tab-item').hide();
        $('div').filter(id).show();
    }
    // Always active tab default
    activeTab($('.tab .tablinks:first-child'));

    $('#login-user').click(function () {
        $('#modal').css({
            "display": "block"
        });
    });

    $('#create-user').click(function () {
        $('#modal').css({
            "display": "block"
        });
        activeTab($('.tab .tablinks:last-child'));
    });
    // Switch tab
    $('.tablinks').click(function () {
        activeTab(this);
        return false;
    });

    $('.forgetpass').click(function () {
        $('#modal').hide();
        $('.forgot-password').css({
            "display": "block"
        });
    });


    $('.search-mobile-btn').click(function () {
        $('#search-form-mobile').slideToggle(fadeTime);
    });

    // hamburger menu 
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
    });
    // Datepicker
    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
    // Input counter item cart
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
    });
    /* Assign actions */
    $('.input-number input').change(function () {
        updateQuantity(this);
    });

    $('.fa-trash-alt').click(function () {
        removeItem(this);
    });

    function recalculateCart() {
        console.log("Phat");
        var subtotal = 0;

        /* Sum up row totals */
        $('.item-row').each(function () {
            subtotal += parseFloat($(this).children('.cart-item').children('.information').children('.item-price').children('#multi').children('.type-price').text());
        });

        /* Calculate totals */
        var shipping = (subtotal > 150.000 ? shippingCharge : 0);
        var total = subtotal + shipping;

        /* Update totals display */
        $('.totals-value .value').fadeOut(fadeTime, function () {
            $('#cart-subtotal .value').html(subtotal.toFixed(3) + "<span> ₫</span>");
            $('#cart-shipping .value').html(shipping.toFixed(3) + "<span> ₫</span>");
            $('#cart-total .value').html(total.toFixed(3) + "<span> (Giá đã bao gồm VAT)</span>").css({ "color": "red", 'font-weight': 'bold' });
            $('#cart-total .value .order-total').css("color", "red");
            if (total == 0) {
                $('.checkout').fadeOut(fadeTime);
            } else {
                $('.checkout').fadeIn(fadeTime);
            }
            $('.totals-value .value').fadeIn(fadeTime);
        });
    }

    /* Update quantity */
    function updateQuantity(quantityInput) {
        /* Calculate line price */
        var productRow = $(quantityInput).parent().parent().parent().parent().parent().parent();
        var price = parseFloat(productRow.children('.cart-item').children('.information').children('.item-price').children('#per').children('.per-price').text());
        var quantity = $(quantityInput).val();
        var linePrice = price * quantity;

        /* Update line price display and recalc cart totals */
        productRow.children('.cart-item').children('.information').children('.item-price').children('#multi').children('.type-price').each(function () {
            $(this).fadeOut(fadeTime, function () {
                $(this).text(linePrice.toFixed(3) + " ₫");
                recalculateCart();
                $(this).fadeIn(fadeTime);
            });
        });
    }
    function removeItem(removeButton) {
        /* Remove row from DOM and recalc cart total */
        var productRow = $(removeButton).parent().parent().parent().parent().parent();
        console.log(productRow)
        productRow.slideUp(fadeTime, function () {
            productRow.remove();
            recalculateCart();
        });
    }

    /* Step events cart */

    $('.step-title').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active').siblings('.step-content').slideToggle();
        $(this).parent().siblings().find('.step-title').removeClass('active').siblings('.step-content').slideUp();
    });
    /* Product-details */

    var urls = [
        './public/img/products/dung-nhin-nua-pha-ra-ong.png',
        './public/img/products/orange-la-thu-tu-tuong-lai-tap-4.png'

    ];

    $('#myGallery').zoomy(urls);

    $('#myGallery').zoomy(urls, {
        width: auto,
    });
});









